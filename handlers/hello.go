package handlers

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

// We will create a struct that implements the interface
type Hello struct {
	l *log.Logger
}

func NewHello(l *log.Logger) *Hello {
	return &Hello{l}
}

func (h *Hello) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	h.l.Println("Hello World!")
	// I/O built it in the IO package.
	d, err := ioutil.ReadAll(r.Body)
	// Handling the error cases and returning a bad req HTTP code.
	if err != nil {
		http.Error(rw, "Something happened", http.StatusBadRequest)
		return
	}

	fmt.Fprintf(rw, "Hello %s\n", d)
}
